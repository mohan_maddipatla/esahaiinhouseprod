//
//  PromosViewController.swift
//  eSahai
//
//  Created by PINKY on 29/03/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

class PromosViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource  {

    @IBOutlet weak var tblPromos: UITableView!
    var arrPromos = NSArray()
    @IBOutlet weak var lblpromos: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Offers"
       // self.getPromos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getPromos()
    }
    
    func getPromos(){
        
        let  strUrl = String(format : "%@%@",kServerUrl,"getpromos") as NSString
        
        let dictParams = ["customerApp": "true",
                          "ambulanceApp": "false",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String]
            as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
            {
                let strStatusCode = result["statusCode"] as? String
                if strStatusCode == "300" {
                    
                    print(result)
                    
                    self.arrPromos = result["Result"] as! NSArray
                    self.tblPromos.reloadData()
                    
                }
                else if strStatusCode == "204"{
                    self.logout()
                }
                else if(strStatusCode != "500"){                    self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                }
            }
        }, failureHandler: {(error) in})
    }
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
}
    
extension PromosViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(arrPromos.count > 0){
            tblPromos.isHidden = false
            lblpromos.isHidden = true
        }else{
            tblPromos.isHidden = true
            lblpromos.isHidden = false
        }
        
        return self.arrPromos.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //tableView.deselectRow(at: indexPath.row , animated: true)
        FBSDKAppEvents.logEvent("Promo Banner Clicked")
        self.tblPromos.deselectRow(at: indexPath, animated: true)
         let vc = self.storyboard?.instantiateViewController(withIdentifier: "PromoDetailViewController") as! PromoDetailViewController
        vc.promodetailsDict = self.arrPromos.object(at: indexPath.row) as! NSDictionary
        vc.isFromDidSelectRowPromoBanner = true
        self.navigationController?.present(vc, animated: true, completion: nil)
        
       // self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell? ?? UITableViewCell(style: .default, reuseIdentifier: "cell")
        
        
        let data = self.arrPromos.object(at: indexPath.row) as! NSDictionary
        
        cell.textLabel?.attributedText = makeAttributedString(promodetails: data)
        
        cell.textLabel?.numberOfLines = 0
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func makeAttributedString( promodetails: NSDictionary) -> NSAttributedString {
       
        let titleAttributes = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: .headline), NSForegroundColorAttributeName: UIColor.init(hex: "0076a7")]
        
        let subtitleAttributes = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: .subheadline), NSForegroundColorAttributeName: UIColor.black]
        
        let firstString = NSMutableAttributedString(string: "Validity : ", attributes: titleAttributes)
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let startDate = dateFormatter1.date(from: (promodetails.value(forKey: "start_date") as! String))
        let endDate = dateFormatter1.date(from: (promodetails.value(forKey: "end_date") as! String))
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd MMM yyyy"
        let strFirst = dateFormatter2.string(from: startDate!)
        let strSecond = dateFormatter2.string(from: endDate!)
        
        let secondString = NSMutableAttributedString(string: "\(strFirst) - \(strSecond)\n", attributes: subtitleAttributes)
    
        
        let thirdSrting = NSMutableAttributedString(string: "\(promodetails .value(forKey: "title")!)\n", attributes: titleAttributes)
        
        let fourthString = NSMutableAttributedString(string: "\(promodetails .value(forKey: "description")!)", attributes: subtitleAttributes)
    
        
        firstString.append(secondString)
        firstString.append(thirdSrting)
        firstString.append(fourthString)
        
        return firstString
    }
}


