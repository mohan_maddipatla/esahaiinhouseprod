//
//  SystemContactsViewController.swift
//  eSahai
//
//  Created by PINKY on 07/03/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit

import Contacts
import ContactsUI

protocol AddContactViewControllerDelegate {
    func didFetchContacts(_ contacts: [CNContact])
}


class SystemContactsViewController: UIViewController, CNContactPickerDelegate {
    
    var delegate: AddContactViewControllerDelegate!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.openContacts()
    }
    
    func openContacts(){
        let contactPickerViewController = CNContactPickerViewController()
        contactPickerViewController.delegate = self
        present(contactPickerViewController, animated: true, completion: nil)
    }
    
    // MARK: CNContactPickerDelegate function
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        delegate.didFetchContacts([contact])
        navigationController?.popViewController(animated: false)
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController){
        navigationController?.popViewController(animated: true)
    }
}
