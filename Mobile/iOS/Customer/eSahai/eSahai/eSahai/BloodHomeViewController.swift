//
//  BloodHomeViewController.swift
//  eSahai
//
//  Created by eSahai on 20/05/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit

class BloodHomeViewController: UIViewController {

    @IBAction func bloodReqTapping(_ sender: Any) {
        let BloodCon = self.storyboard?.instantiateViewController(withIdentifier:"BloodRequestViewController")
        self.navigationController?.pushViewController(BloodCon!, animated: true)
    }
    
    @IBAction func viewBloodBanksTapping(_ sender: Any) {
        let BloodCon = self.storyboard?.instantiateViewController(withIdentifier:"BloodBanksListViewController")
        self.navigationController?.pushViewController(BloodCon!, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
