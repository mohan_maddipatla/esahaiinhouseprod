//
//  EmergencyRequestsViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/9/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit

class EmergencyInfoTableViewCell: UITableViewCell {
    
    @IBOutlet var cosmosRatingViewObj: CosmosView!
    @IBOutlet var btnRate_Outlet: UIButton!
    @IBOutlet var lblCost: UILabel!
    @IBOutlet var imgBookingCanceled: UIImageView!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var lblAmbulanceNumber: UILabel!
    @IBOutlet var lblHospitalName: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblBookingId: UILabel!
    @IBOutlet var lblBookedTime: UILabel!
    @IBOutlet var imgDriver: UIImageView!
    
    
    var driverRatingViewObj = UIView()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgDriver.layer.masksToBounds = false
        imgDriver.layer.borderColor = UIColor.white.cgColor
        imgDriver.layer.cornerRadius = 25.0
        imgDriver.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class TripsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate , delegatePopOver {
    
    var arrResponseData = NSMutableArray()
    var feedback = NSMutableDictionary()
    @IBOutlet var tableViewObj: UITableView!
    
    @IBOutlet weak var labelText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My Emergencies"
        tableViewObj.tableFooterView = UIView()
        self.getEmergenciesInfo()
        //For Rounded Image View
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Slide Navigation

 func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
    return true
 }
    func getEmergenciesInfo()
    {
        let  strUrl = String(format : "%@%@",kServerUrl,"getEmergencies") as NSString
        
        let dictParams = ["customerApp": "true","customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,kCustomer_id:UserDefaults.standard.value(forKey: kCustomer_id)as! String] as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        self.arrResponseData = result.value(forKey: "responseData") as! NSMutableArray
                        
                        print(self.arrResponseData)
                        
                        for i in 0..<self.arrResponseData.count{
                            let tempDict = self.arrResponseData.object(at: i) as! NSMutableDictionary
                            
                            for (key, value) in tempDict {
                                print("value --> \(value)" )
                                if value is NSNull{
                                    tempDict[key] = ""
                                    self.arrResponseData.replaceObject(at: i, with: tempDict)
                                }else if(value is String){
                                    if(value as! String == " "){
                                        tempDict[key] = ""
                                        self.arrResponseData.replaceObject(at: i, with: tempDict)
                                    }
                                }
                            }
                        }
                        
                        
                        self.tableViewObj.reloadData()
                        
                    }
                    else if strStatusCode == "204"{
                        self.logout()
                    }
                    else
                    {
                        //self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                    }
            }
            }, failureHandler: {(error) in
        })
    }
    
    // MARK: - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(arrResponseData.count > 0){
            self.labelText.isHidden = true
            self.tableViewObj.isHidden = false
        }else{
            self.labelText.isHidden = false
            self.tableViewObj.isHidden = true
        }
        return arrResponseData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var emergencyInfoTableCell = tableView.dequeueReusableCell(withIdentifier: "EmergencyInfoTableViewCell")as! EmergencyInfoTableViewCell
        
        if emergencyInfoTableCell == nil {
            emergencyInfoTableCell = UITableViewCell.init(style: .default, reuseIdentifier: "EmergencyInfoTableViewCell") as! EmergencyInfoTableViewCell
        }
        let tempDict = arrResponseData.object(at: indexPath.row) as! NSDictionary
    
        
       // emergencyInfoTableCell.lblBookedTime.text = tempDict.value(forKey: "created_datetime") as? String
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let startDate = dateFormatter1.date(from: (tempDict.value(forKey: "created_datetime") as? String)!)
      
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd MMM yyyy hh:mm a"
        let strFirst = dateFormatter2.string(from: startDate!)
      
        
        emergencyInfoTableCell.lblBookedTime.text = strFirst
        
        
        
        
        
        emergencyInfoTableCell.lblBookingId.text = String(format :" Booking Id: %@",(tempDict.value(forKey: "booking_id") as? String)!)
        emergencyInfoTableCell.lblAddress.text = tempDict.value(forKey: "eme_address") as? String
        emergencyInfoTableCell.lblHospitalName.text = tempDict.value(forKey: "hospital") as? String
        emergencyInfoTableCell.lblAmbulanceNumber.text = tempDict.value(forKey: "ambulance_number") as? String
        emergencyInfoTableCell.lblDriverName.text = tempDict.value(forKey: "driver_name") as? String
      print(tempDict)
        if !(tempDict.value(forKey: "booking_status")as? String == kCancelCode){
            
            if !(tempDict.value(forKey: "booking_status")as? String == kFakeCancelCode){
            //||
            //!(tempDict.value(forKey: "booking_status")as? String == "CANCFAKE") {
            //(tempDict.value(forKey: "booking_status")as? String != kCancelCode) || (tempDict.value(forKey: "booking_status")as? String != kFakeCancelCode) {
            
            emergencyInfoTableCell.lblCost.isHidden = false
            
            emergencyInfoTableCell.imgBookingCanceled.isHidden = true
            
            if((tempDict.value(forKey: "feedback")) != nil){
                feedback = tempDict.value(forKey: "feedback") as! NSMutableDictionary
                if feedback.count > 0
                {
                    emergencyInfoTableCell.cosmosRatingViewObj.isHidden = false
                    emergencyInfoTableCell.cosmosRatingViewObj.rating = Double(feedback.value(forKey: "points") as! String)!
                    emergencyInfoTableCell.btnRate_Outlet.isHidden = true
                }
                
            }else {
                feedback.removeAllObjects()
                emergencyInfoTableCell.btnRate_Outlet.isHidden = false
                emergencyInfoTableCell.btnRate_Outlet.isHidden = false
                emergencyInfoTableCell.cosmosRatingViewObj.rating = 0.0
            }
           // emergencyInfoTableCell.cosmosRatingViewObj.rating = 2.0
                if((tempDict.value(forKey: "cost") as? String) != ""){
                    emergencyInfoTableCell.lblCost.text = String(format : "₹%@",(tempDict.value(forKey: "cost") as? String)!)
                }else{
                    emergencyInfoTableCell.lblCost.isHidden = true
                     emergencyInfoTableCell.cosmosRatingViewObj.isHidden = true
                    emergencyInfoTableCell.btnRate_Outlet.isHidden = true
                }
            }
        }
        else{
            emergencyInfoTableCell.imgBookingCanceled.isHidden = false
          //  emergencyInfoTableCell.cosmosRatingViewObj.rating = 0.0
            emergencyInfoTableCell.lblCost.isHidden = true
            emergencyInfoTableCell.btnRate_Outlet.isHidden = true
            emergencyInfoTableCell.cosmosRatingViewObj.isHidden = true
            emergencyInfoTableCell.imgDriver.image = UIImage.init(named:"mine.png")
        }
        
        LazyImage.show(imageView:emergencyInfoTableCell.imgDriver, url:tempDict.value(forKey: "driver_profile") as? String) {
            () in
            //Lazy loading complete. Do something..
        }
        
      //  let btnRate = emergencyInfoTableCell.viewWithTag(111) as! UIButton
        emergencyInfoTableCell.btnRate_Outlet.tag = indexPath.row
        emergencyInfoTableCell.btnRate_Outlet.addTarget(self, action: #selector(self.btnRateClicked(sender:)), for:.touchUpInside)
        return emergencyInfoTableCell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableViewObj.deselectRow(at: indexPath, animated: true)
        
        let detailedListCon = self.storyboard?.instantiateViewController(withIdentifier: "DetailedEmergiencyInfoViewController") as! DetailedEmergiencyInfoViewController
        
        detailedListCon.dictDetails = self.arrResponseData.object(at: indexPath.row) as! NSDictionary
        print(detailedListCon.dictDetails)
        
        if  (detailedListCon.dictDetails.value(forKey: "booking_status")as? String == "CAN") || (detailedListCon.dictDetails.value(forKey: "booking_status")as? String == "CANCFAKE") {
            
        }else if((detailedListCon.dictDetails.value(forKey: "cost") as? String) == ""){
        }
        else{
           self.navigationController?.pushViewController(detailedListCon, animated: true)
        }
    }
    
    func btnRateClicked(sender : UIButton!) {
        
        let popoverContent = (self.storyboard?.instantiateViewController(withIdentifier: "RatingPopOverViewController")) as! RatingPopOverViewController
        popoverContent.IsFromRATEClicked = true
        popoverContent.bookingID = (arrResponseData.object(at: sender.tag) as! NSDictionary).value(forKey: "booking_id") as! String
        popoverContent.del = self
        let nav = UINavigationController(rootViewController: popoverContent)
        nav.modalPresentationStyle = UIModalPresentationStyle.popover
        let popover = nav.popoverPresentationController
        popoverContent.preferredContentSize = CGSize(width:320, height:130)
        popover?.delegate = self
        popover?.sourceView = self.view
        popover?.sourceRect = CGRect(x: 100, y: 80, width: 0, height: 0)
        self.present(nav, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
    
    func updateTable() {
        self.getEmergenciesInfo()
    }
}
