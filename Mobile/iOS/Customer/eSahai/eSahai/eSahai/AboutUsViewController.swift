//
//  AboutUsViewController.swift
//  eSahai
//
//  Created by PINKY on 01/03/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit
import MessageUI


class AboutUsViewController: BaseViewController , MFMailComposeViewControllerDelegate {

    @IBOutlet weak var lblVersion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "About Us"
        lblVersion.text = String(format:"V - %@",appDelegate.strAppVersion)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Slide Navigation
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    
    @IBAction func btnPhone(_ sender: Any) {
        guard let number = URL(string: "telprompt://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)")
            else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(number, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
            if let url = URL(string: "tel://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)") {
                UIApplication.shared.openURL(url)
            }
        }
    }

    @IBAction func btnEmail(_ sender: Any) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.setToRecipients(["info@esahai.in"])
        mailComposerVC.setSubject("eSahai Customer - Feedback")
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let strCurrentDate = formatter.string(from: date)
        
        let strBody : String = String(format :"\n\n\n\n\n\n\n-----------------------------------The data below will be used for complaint resolution. Please do not delete any information.\n\nMobile Number: %@.\nHandset Make: %@.\nModel: %@.\nOS Version: %@.\nDate & Time: %@.\n-----------------------------------",UserDefaults.standard.value(forKey: kMobileNumber) as! String,"Apple",appDelegate.getModel(),appDelegate.strDeviceOSVersion,strCurrentDate) as String

        
        mailComposerVC.setMessageBody(strBody, isHTML: false)

        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?){
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnWebSite(_ sender: Any) {
        UIApplication.tryURL(urls: [
            "http://www.esahai.in/" // Website if app fails
            ])
    }
    
    @IBAction func btnFacebook(_ sender: Any) {
        print("inside facebook")
        UIApplication.tryURL(urls: [
            "fb://profile/esahaiindia", // App
            "https://www.facebook.com/esahaiindia" // Website if app fails
            ])
    }
    
    @IBAction func btnInsta(_ sender: Any) {
        print("inside instagram")
        UIApplication.tryURL(urls: [
            "instagram://user?username=esahaiindia", // App
            "https://www.instagram.com/esahaiindia" // Website if app fails
            ])
    }
    
    @IBAction func btnTwitter(_ sender: Any) {
        print("inside Twitter")
        UIApplication.tryURL(urls: [
        "twitter://user?screen_name=esahaiindia", // App
        "https://www.twitter.com/esahaiindia" // Website if app fails
        ])
    }
}

extension UIApplication {
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
       // DispatchQueue.main.async {
            for url in urls {
                if let urlString = URL(string: url), application.canOpenURL(urlString) {
                    application.openURL(urlString)
                    return
                }
            }
      //  }
        
    }
}
