//
//  UIView+RemoveConstraints.h
//  WifiHotspots
//
//  Created by SudheerPalchuri on 25/09/15.
//  Copyright (c) 2015 Tata Teleservices. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RemoveConstraints)
- (void)removeAllConstraints;
@end
